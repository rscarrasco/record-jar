import re

def parse(recordjar, separator="%%"):
    """
    Parses a record-jar string

    The Record-Jar is a simple data file format, proposed by
    Eryc Raymond at his book The Art of Unix Programming 
    (chap 5). Check it out at 
    http://catb.org/esr/writings/taoup/html/ch05s02.html.

    Parameters:
        recordjar (str): the string to be parsed
        separator (str): record separator

    Returns:
        The record jar data as an array of dictionaries.
    """
    if type(recordjar) != str:
        raise TypeError("The record jar must be a string")
    if type(separator) != str:
        raise TypeError("The separator must be a string")
    records = []
    current = {}
    dirty = False
    sep = re.compile("^"+re.escape(separator))
    empty = re.compile("^\\s*$")
    kvsep = re.compile("\:")
    for line in recordjar.split("\n"):
        if sep.match(line):
            records.append(current)
            current = {}
            dirty = False
        elif empty.match(line):
            pass
        else:
            key, value = kvsep.split(line)
            key, value = key.strip(), value.strip()
            # The following try block was shamelessly stolen from
            # https://code.activestate.com/recipes/436229-record-jar-parser/
            try:
                current[key].append(value)
            except AttributeError:
                current[key] = [current[key], value]
            except KeyError:
                current[key] = value
            dirty = True
    if dirty:
        records.append(current)
    return records
